package FourWeek;

class Point {
	private int x, y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	protected void move(int x, int y) {
		this.x = x;
		this.y = y;
	}
}

public class ColorPoint extends Point{
	private String str;
	
	public ColorPoint(int x, int y, String str) {
		super(x,y);
		this.str = str;
	}
	
	public void setPoint(int x, int y) {
		move(x,y);
	}
	
	public void setColor(String str) {
		this.str = str;
	}
	
	public void show() {
		System.out.println(str+"색으로("+getX()+","+getY()+")");
	}
	public static void main(String[] args) {
		ColorPoint cp = new ColorPoint(5, 5, "YELLOW");
		cp.setPoint(10, 20);
		cp.setColor("GREEN");
		cp.show();
	}
}
