package PersonalProject;

import java.util.HashMap;

public class InedibleFood implements AnimalFood{

	@Override
	public boolean Food(String food) { //먹을 수 있는지 검사
		String[] foodList = new String[] { "초콜렛", "양파", "마늘", "포도", "아보카도", "뼈"};
		boolean test = false;
		for (String f : foodList) {
			if (f.equals(food)) {
				test = true;
			}
		}
		return test;

	}

	@Override
	public String Nutrition(String food, String processed) {
		HashMap<String, String> NutritionList = new HashMap<String, String>();
		NutritionList.put("초콜렛", "반려동물에게 주면 안되는 대표적인 음식입니다. 카카오는 카페인과 테오브로민이라는 성분을 함유하여 동물들에게 치명적인 음식입니다.");
		NutritionList.put("양파", "위장장애(구토, 설사, 식욕부진, 우울, 탈진)와 혈액의 손실로 인해 호흡곤란, 소변색 이상 등이 발생합니다.");
		NutritionList.put("마늘", "위장장애(구토, 설사, 식욕부진, 우울, 탈진)와 혈액의 손실로 인해 호흡곤란, 소변색 이상 등이 발생합니다.");
		NutritionList.put("포도", "24시간안에 급성신부전을 일으켜 생명을 잃게 할 수 있습니다. 섭취 했다면 강제 구토 후, 동물병원에 내원하십시오.");
		NutritionList.put("아보카도", "펄신이라는 독성 물질로 인해 설사, 구토, 호흡 곤란 등이 일어날 수 있습니다.");
		NutritionList.put("뼈", "삶거나 익힌 뼈는 잘 부서져 목에 걸려 질식할 수 있습니다. 또한 소화기관을 손상시킬 수 있으므로 급여하지 마십시오.");
		
		String process = null;
		for (String n : NutritionList.keySet()) {
			if (processed.equals(n)) {
				process = NutritionList.get(n).toString();
				break;
			}
		}
		return process;
	}
	
}
