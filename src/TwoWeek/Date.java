package TwoWeek;

public class Date {
	int year;
	String month;
	int day;
	
	public Date(int i, String string, int j) {
		this.year = i;
		this.month = string;
		this.day = j;
	}
	
	public Date(int i) {
		this.year = i;
		this.month = "1월";
		this.day = 1;
	}
	
	public Date() {
		this.year = 1900;
		this.month = "1월";
		this.day = 1;
	}

	public String toString() {
		return "Data [year=" + year + ", month=" + month + ", day=" + day + "]";
	}
}
