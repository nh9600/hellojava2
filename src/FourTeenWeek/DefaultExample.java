package FourTeenWeek;

public class DefaultExample {
	public static void main(String[] args) {
		MyInterface mi = new MyClassA();
		mi.method1();
		mi.method2();
		
		MyInterface mi2 = new MyClassB();
		mi2.method1();
		mi2.method2();
	}

}
