package FourTeenWeek;

public class CarExample {
	public static void main(String[] args) {
		Car myCar = new Car();
		//myCar.run();
		
		/*
		 * myCar.fronLeftTire = new KumhoTire(); 
		 * myCar.fronRightTire = new KumhoTire();
		 */
		myCar.tires[0] = new KumhoTire();
		myCar.run();
	}

}
