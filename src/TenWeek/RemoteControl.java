package TenWeek;

public interface RemoteControl {
	
	//상수
	int MAX_VOLUME = 10;
	int MIN_VOLUME = 10; 
	
	//추상 메소드
	void turnOn();
	void turnOff();
	void setVolume(int volume);
	
	//디폴트 메소드
	default void setVolume(boolean mute) {
		if(mute) {
			System.out.println("무음");
		}else {
			System.out.println("소음");
		}
	}
	
	//정적 메소드
	static void changeBattery() {
		System.out.println("건전지 교체");
	}
	

}
