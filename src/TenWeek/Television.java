package TenWeek;

public class Television implements RemoteControl{
	
	//필드
	private int volume;
	
	@Override
	public void turnOn() { // 추상메소드의 실체 메소드 
		System.out.println("tv on")
		;
	}

	@Override
	public void turnOff() { // 추상메소드의 실체 메소드 
		System.out.println("tv off")
		;
		
	}

	@Override
	public void setVolume(int volume) {
		if(volume > RemoteControl.MAX_VOLUME) {
			this.volume = RemoteControl.MAX_VOLUME;
		}else {
			this.volume = RemoteControl.MIN_VOLUME;
		}
		System.out.println("현자 볼륨" + volume);
	}
	

}
