package ThirteenWeek;

import java.io.IOException;

public class KeyboardToStringExample {
	public static void main(String[] args) throws IOException {
		byte[] bytes = new byte[100];
		
		System.out.println("입력:");
		int readByteNo = System.in.read(bytes); //아스키 코드
		
		String str = new String(bytes, 0, readByteNo-2); //엔터키인 \r \n은 문자열로 필요없음
		System.out.println(str);
		
	}

}
