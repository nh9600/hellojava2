package TenWeek.homework;

public class MyAdder implements AdderInterface{
	
	private int hap;
	
	@Override
	public int add(int x, int y) {
		
		return x + y;
	}

	@Override
	public int add(int x) {
		for(int y=1;y<=x;y++) {
			hap += y;
		}
		return hap;
	}
	
	public static void main(String[] args) {
		MyAdder add = new MyAdder();
		System.out.println(add.add(4));
		System.out.println(add.add(4,1));
	}

}
