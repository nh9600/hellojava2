package FourTeenWeek;

public class Car {
	Tire fronLeftTire = new HankookTire();
	Tire fronRightTire = new HankookTire();
	Tire backLeftTire = new KumhoTire();
	Tire backRightTire = new KumhoTire();
	
	void run() {
	/*	fronLeftTire.roll();
		fronRightTire.roll();
		backLeftTire.roll();
		backRightTire.roll();*/
		
		for(Tire tire : tires) {
			tire.roll();
		}
	}
	
	Tire[] tires = {
			new HankookTire(),
			new HankookTire(),
			new KumhoTire(),
			new KumhoTire()
	};
	
}
