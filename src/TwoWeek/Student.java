package TwoWeek;

public class Student {
	int number;
	String name;
	int age;
	
	public Student() {
		this.number = 100;
		this.name = "New Student";
		this.age = 18;
	}
	
	public Student(int i, String string, int j) {
		this.number = i;
		this.name = string;
		this.age = j;
	}

	public String toString() {
		return "Student [number=" + number + ", name=" + name + ", age=" + age + "]";
		}

}
