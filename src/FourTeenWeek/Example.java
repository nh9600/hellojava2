package FourTeenWeek;

public class Example {
	public static void main(String[] args) {
		Implementation imple = new Implementation();
		
		InterfaceA ia = imple;
		ia.methodA();
		System.out.println();
		
		InterfaceB ib = imple;
		ib.methodB();
		System.out.println();
		
		InterfaceC ic = imple;
		ic.methodC();
		ic.methodA();
		ic.methodB();
		System.out.println();
	}

}
