package FourWeek;

class Circle {
	private int radius;

	public Circle(int radius) {
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}
}

public class NameCircle extends Circle {
	private String str;
	
	public NameCircle(int radius, String str) {
		super(radius);
		this.str = str;
	}

	public void show() {
		System.out.println(str + ", " + "반지름 = " + getRadius());
	}

	public static void main(String[] args) {
		NameCircle w = new NameCircle(5, "Waffle");// NamedCircle(int r, String n) 생성자 작성
		w.show();// NamedCircle에 show()를 작성
	}
}