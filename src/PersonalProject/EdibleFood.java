package PersonalProject;

import java.util.HashMap;

public class EdibleFood implements AnimalFood {

	@Override
	public boolean Food(String food) { //먹을 수 있는지 검사
		String[] foodList = new String[] { "고구마", "감자", "옥수수" };
		boolean test = false;
		for (String f : foodList) {
			if (f.equals(food)) {
				test = true;
			}
		}
		return test;

	}
	
	//가공상태 검사
	public String Food1(String food) { 
		HashMap<String, String> processList = new HashMap<String, String>();
		processList.put("감자", "구운것, 삶은것, 생것, 찐것");
		processList.put("고구마", "구운것, 마른것, 생것, 찐것");
		processList.put("옥수수", "생것, 마른것, 찐것");
		
		String process = null;
		for (String f : processList.keySet()) {
			if (food.equals(f)) {
				process = processList.get(f).toString();
				break;
			}
		}
		return process;

	}
	
	@Override
	public String Nutrition(String food, String processed) {
		if(food.equals("감자")) {
			return Potato(processed);
		}else if(food.equals("고구마")) {
			return SweetPotato(processed);
		}else if(food.equals("옥수수")){
			return Corn(processed);
		}
		return "에러";
	}
	
	//고구마 영양소
	public String SweetPotato(String processed) {
		HashMap<String, String> NutritionList = new HashMap<String, String>();
		NutritionList.put("구운것", "수분(%):68.0, 단백질(%):1.50, 칼륨(%):0.44, 지방(%):0.20, 탄수화물(%):29.30");
		NutritionList.put("마른것", "수분(%):19.60, 단백질(%):2.30, 칼륨(%):0.99, 지방(%):0.10, 탄수화물(%):75.70");
		NutritionList.put("생것", "수분(%):66.30	, 단백질(%):1.40, 칼륨(%):0.43, 지방(%):0.20, 탄수화물(%):31.20");
		NutritionList.put("찐것", "수분(%):80.60, 단백질(%):3.00, 칼륨(%):0.55, 탄수화물(%):31.30");
		
		String process = null;
		for (String n : NutritionList.keySet()) {
			if (processed.equals(n)) {
				process = NutritionList.get(n).toString();
				break;
			}
		}
		return process;
	}
	
	//감자 영양소
	public String Potato(String processed) {
		HashMap<String, String> NutritionList = new HashMap<String, String>();
		NutritionList.put("구운것", "수분(%):80.80, 단백질(%):1.80, 칼륨(%):0.36, 지방(%):0.1, 탄수화물(%):16.40");
		NutritionList.put("삶은것", "수분(%):81.50, 단백질(%):3.20, 칼륨(%):0.48, 지방(%):0.0, 탄수화물(%):14.20");
		NutritionList.put("생것", "수분(%):82.70, 단백질(%):2.40, 칼륨(%):0.56, 지방(%):0.0, 탄수화물(%):13.90");
		NutritionList.put("찐것", "수분(%):80.60, 단백질(%):3.0, 칼륨(%):0.48, 지방(%):0.0, 탄수화물(%):15.30");
		
		String process = null;
		for (String n : NutritionList.keySet()) {
			if (processed.equals(n)) {
				process = NutritionList.get(n).toString();
				break;
			}
		}
		return process;
	}
	
	//옥수수 영양소
	public String Corn(String processed) {
		HashMap<String, String> NutritionList = new HashMap<String, String>();
		NutritionList.put("마른것", "수분(%):7.50, 단백질(%):11.50, 칼륨(%):0.18, 지방(%):4.60, 탄수화물(%):74.70");
		NutritionList.put("생것", "수분(%):63.60, 단백질(%):4.90, 칼륨(%):0.37, 지방(%):1.20, 탄수화물(%):29.40");
		NutritionList.put("찐것", "수분(%):65.70, 단백질(%):3.0, 칼륨(%):0.33, 지방(%):1.40, 탄수화물(%):25.40");
		
		String process = null;
		for (String n : NutritionList.keySet()) {
			if (processed.equals(n)) {
				process = NutritionList.get(n).toString();
				break;
			}
		}
		return process;
	}
	
	//하루 급여량
	public double OneDay(int kg, int num2) {
		int per = 0; // 최소 열량
		double der = 0; //하루 필요 열량 
		double feed = 0; //하루 사료양
		double feedM = 0; //한달 사료양
		
		per = 30 * kg + 70; 
		switch (num2) {
		case 1 : 
			der = per * 0.8; //비만
			break;
		case 2 : 
			der = per * 1.2; //중성화 
			break;
		case 3 : 
			der = per * 1.4; //중성화X
			break;
		case 4 : 
			der = per * 1.6; //활동 많음
			break;
		}
		
		feed = der / 4; // 1kg 4000kcal기준
		return feed;
	}

}
