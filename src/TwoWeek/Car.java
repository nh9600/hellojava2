package TwoWeek;

public class Car {
	int gear;
	String color;
	int speed;
	
	public void changeGear(int x) {
		this.gear = x;
	}
	
	public void changeColor() {
		this.color = null;
	}
	
	public void speedUp() {
		this.speed = 10;
	}
	
	public String toString(){
		return "Car [color=" + color + ", speed=" + speed + ", gear=" + gear + "]"; 
    }

}
