package NineWeek;

public class AnimalExample {

	public static void main(String[] args) {
		Dog dog = new Dog();
		Cat cat = new Cat();
		//dog.sound();
		//cat.sound();
		
		//변수의 자동 타입 변환
		Animal animal = null;
		animal = new Dog();
		//animal.sound();
		animal = new Cat();
		//animal.sound();
		animalSound(new Dog());
		animalSound(new Cat());

	}

	//개, 고양이 모두 호출
	private static void animalSound(Animal animal) {
		animal.sound();
		
	}

}
