package PersonalProject;

import java.util.Scanner;

public class PetNutritionManagement {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num1; // 번호 선택
		String food; // 음식
		String processed; // 가공상태

		AnimalFood aFood; // 음식 감별
		EdibleFood eFood = new EdibleFood(); // 가공상태 확인

		while (true) {
			System.out.println("1. 반려동물 영양 비율");
			System.out.println("2. 궁금한 음식[보기 : 감자, 고구마, 옥수수, 포도, 초콜렛, 양파, 마늘, 아보카도, 뼈]");
			System.out.println("3. 체중별 사료의 적정량");
			System.out.print("번호를 선택해주세요 :");
			num1 = scan.nextInt();
			if (num1 == 1) {
				System.out.println("음식은 체중의 5~8%를 주는 것이 원칙이며 영양학적으로 " 
											+ "단백질 25%, 탄수화물 50%, 지방 8%의 비율을 유지해야 합니다.\n");

			} else if (num1 == 2) {

				System.out.print("궁금한 음식을 입력해주세요 : ");
				food = scan.next();

				aFood = new EdibleFood();// 먹을 수 있는 음식

				if (aFood.Food(food)) {
					while (true) {
						System.out.print("[" + eFood.Food1(food) + "]가공 상태를 입력해주세요 :");
						processed = scan.next();

						if (processed.equals("구운것")) {
							System.out.println("kg기준 : " + eFood.Nutrition(food, processed) + "\n");
							break;
						} else if (processed.equals("마른것")) {
							System.out.println("kg기준 : " + eFood.Nutrition(food, processed) + "\n");
							break;
						} else if (processed.equals("생것")) {
							System.out.println("kg기준 : " + eFood.Nutrition(food, processed) + "\n");
							break;
						} else if (processed.equals("찐것")) {
							System.out.println("kg기준 : " + eFood.Nutrition(food, processed) + "\n");
							break;
						} else if (processed.equals("삶은것")) {
							System.out.println("kg기준 : " + eFood.Nutrition(food, processed) + "\n");
							break;
						} else {
							System.out.println("보기에 없는 가공 상태입니다. 다시 입력해주세요.");
						}
					}
				} else {
					aFood = new InedibleFood();// 먹을 수 없는 음식

					if (aFood.Food(food)) {
						System.out.println(aFood.Nutrition(food, food) + "\n");
					} else {
						System.out.println(food + "는(은) 보기에 없는 음식입니다.\n");// 보기에 없을 경우
					}
				}
			} else if (num1 == 3) {
				int kg = 0;
				int num2 = 0;
				double oneday; // 하루 급여
				double month; // 한달

				System.out.println("\n1.감량이 필요 2.중성화 수술 받음 3.중성화 수술 안함 4.운동량이 많고 활발함");
				System.out.print("해당되는 번호 1개를 입력해주세요 :");
				num2 = scan.nextInt();

				System.out.print("고양이의 체중을 입력해주세요 :");
				kg = scan.nextInt();

				oneday = eFood.OneDay(kg, num2);
				month = oneday * 30;

				System.out.println("하루 급여량 : " + oneday + "g\n한달 30일 기준 : " + month + "g\n");

			} else {
				System.out.println("보기에 없는 번호입니다.");
			}
		}
	}
}
