package TenWeek;

public class Audio implements RemoteControl{
	
	private int volume;

	@Override
	public void turnOn() {
		System.out.println("오디오 킴");
		
	}

	@Override
	public void turnOff() {
		System.out.println("오디오 끔");
		
	}

	@Override
	public void setVolume(int volume) {
		if(volume > RemoteControl.MAX_VOLUME) {
			this.volume = RemoteControl.MAX_VOLUME;
		}else {
			this.volume = RemoteControl.MIN_VOLUME;
		}
		System.out.println("현자 볼륨" + volume);
	}
	
	

}
