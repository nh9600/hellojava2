package NineWeek;

class Adder extends Calculator{
	protected int calc() {
		return a+b;
	}
	
}

class Subtracter extends Calculator{
	protected int calc() {
		return a-b;
	}
	
}
public class App {
	
	public static void main(String[] args) {
		Adder adder = new Adder();
		Subtracter sub = new Subtracter();
		adder.run();
		sub.run();
		}
}