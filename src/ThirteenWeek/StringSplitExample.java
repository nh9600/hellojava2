package ThirteenWeek;

import java.util.StringTokenizer;

public class StringSplitExample {
	public static void main(String[] args) {
		String text = "홍길동/이수홍/박연수";

		// how1:전체토큰수를얻어for문으로루핑
		StringTokenizer st = new StringTokenizer(text, "/");
		int countTokens = st.countTokens();
		for (int i = 0; i < countTokens; i++) {
			String token = st.nextToken();//토큰 읽어오기
			System.out.println(token);
		}
		System.out.println();

		// how2:남아있는토큰를확인하고while문으로루핑
		st = new StringTokenizer(text, "/");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			System.out.println(token);
		}
	}

}
